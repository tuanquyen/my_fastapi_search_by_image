import pickle

def check_size():
    try:
        with open("vectors.pkl", "rb") as vectors_file:
            vectors = pickle.load(vectors_file)
            vectors_count = len(vectors)
        print(f"Number of objects in vectors.pkl: {vectors_count}")
    except FileNotFoundError:
        print("vectors.pkl not found")

    try:
        with open("name_images.pkl", "rb") as names_file:
            names = pickle.load(names_file)
            names_count = len(names)
        print(f"Number of objects in name_images.pkl: {names_count}")
    except FileNotFoundError:
        print("name_images.pkl not found")

if __name__ == "__main__":
    check_size()

from fastapi import FastAPI, File, Query, UploadFile, Request, HTTPException
import pickle
from fastapi.responses import JSONResponse
from typing import List
from io import BytesIO
from image_processor import extract_vector, get_extract_model
import numpy as np
import requests
import base64
from fastapi.middleware.cors import CORSMiddleware

from PIL import Image

app = FastAPI()

origins = ["*"]

# Đọc dữ liệu hiện tại từ vectors.pkl và images_id.pkl
try:
    with open("vectors.pkl", "rb") as vectors_file:
        vectors = pickle.load(vectors_file)
except FileNotFoundError:
    vectors = []

try:
    with open("images_id.pkl", "rb") as id_file:
        ids = pickle.load(id_file)
except FileNotFoundError:
    ids = []
    
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
) 

# Khoi tao model
model = get_extract_model()

@app.post("/upload/")
async def upload_files(images_id: List[str], locations: List[str]):
    try:
        if not locations:
            return JSONResponse(status_code=400, content={"message": "No locations provided"})
    
        if len(locations) != len(images_id):
            return JSONResponse(status_code=400, content={"message": "Number of locations and images_id do not match"})
        
        existing_ids = set(ids)

        for id in images_id:
            # Kiểm tra xem id đã tồn tại trong existing_ids chưa
            if id in existing_ids:
                print(f"Image ID '{id}' already exists. Skipping.")
                return JSONResponse(status_code=400, content={"message": f"Image ID '{id}' already exists."})
            

        for location, id in zip(locations, images_id):
            print('location:', location)
            print('id:', id)
            img_buffer = download_image_from_url(location)
            if img_buffer is not None:
                vector = extract_vector(model, img_buffer)
                vectors.append(vector)
                ids.append(id)
    
        # Save vectors to vectors.pkl and ids to images_id.pkl
        with open("vectors.pkl", "wb") as vectors_file:
            pickle.dump(vectors, vectors_file)

        with open("images_id.pkl", "wb") as id_file:
            pickle.dump(ids, id_file)

        return JSONResponse(status_code=200, content={"success": True})
    except Exception as e:
        print(e)
        return JSONResponse(status_code=400, content={"success": False}) 
    

def download_image_from_url(url):
    try:
        response = requests.get(url)
        img_data = response.content
        return img_data
    except Exception as e:
        print(f"Error downloading image from URL: {e}")
        raise ValueError(f"Error downloading image from URL: {e}")


# API endpoint để xóa dữ liệu vector và image_id dựa trên id ảnh
@app.delete("/delete/")
async def delete_image(id: str = Query(...)):
    try:
        print("id:", id)
        index = ids.index(id)
        del vectors[index]
        del ids[index]

        # Lưu lại các danh sách sau khi xóa
        with open("vectors.pkl", "wb") as vectors_file:
            pickle.dump(vectors, vectors_file)

        with open("images_id.pkl", "wb") as id_file:
            pickle.dump(ids, id_file)

        return JSONResponse(status_code=200, content={"success": True})
    except ValueError as e:
        print(e)
        return JSONResponse(status_code=400, content={"success": False, "data": "Image not found."})

max_distance = 1.2

# API endpoint để tìm kiếm các ảnh tương đồng dựa trên một tấm ảnh đầu vào
@app.post("/search/")
async def search_similar_images(image_file: UploadFile = File(...)):
    try:
        # Đọc và xử lý ảnh đầu vào
        search_image = await image_file.read()
        search_vector = extract_vector(model, search_image)

        # Tính toán khoảng cách giữa vector của ảnh đầu vào và các vector trong vectors.pkl
        distance = np.linalg.norm(vectors - search_vector, axis=1)
        K = 4
        similar_images = []
        for i, dist in enumerate(distance):
            if dist < max_distance:
                similar_images.append((ids[i], dist))
                print(f"Image ID: {ids[i]}, Distance: {dist}")


        # Sắp xếp các ảnh tương tự theo khoảng cách
        similar_images.sort(key=lambda x: x[1])
        
        K = min(len(similar_images), 4) 
        similar_images = [img_id for img_id, _ in similar_images[:K]]
        
        print(similar_images)
    

        return JSONResponse(status_code=200, content={"similar_images_id": similar_images})
    except Exception as e:
        print(e)
        return HTTPException(status_code=400, content={"success": False})
    

@app.post("/search_base64/")
async def search_similar_images_base64(request: Request):
    try:
        data = await request.json()
        image_base64 = data.get("image_base64")
        if not image_base64:
            raise HTTPException(status_code=400, detail="Missing 'image_base64' in request body")
        # Giải mã dữ liệu base64 thành ảnh nhị phân
        search_image = base64.b64decode(image_base64)
        
        # # Xử lý ảnh và tìm kiếm ảnh tương tự
        search_vector = extract_vector(model, search_image)
        distance = np.linalg.norm(vectors - search_vector, axis=1)
        K = 4
        # ids_result = np.argsort(distance)[:K]
        # similar_images = [ids[i] for i in ids_result]
        
        similar_images = []
        for i, dist in enumerate(distance):
            if dist < max_distance:
                similar_images.append((ids[i], dist))
                print(f"Image ID: {ids[i]}, Distance: {dist}")


        # Sắp xếp các ảnh tương tự theo khoảng cách
        similar_images.sort(key=lambda x: x[1])
        
        K = min(len(similar_images), 4) 
        similar_images = [img_id for img_id, _ in similar_images[:K]]
        
        print(similar_images)
        return JSONResponse(status_code=200, content={"similar_images_id": similar_images})
    except Exception as e:
        print(e)
        return HTTPException(status_code=400, detail="Search failed")

@app.get("/")
async def root():
    return {"message": "Hello World"}

